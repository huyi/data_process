
#include <TH2.h>
#include <TStyle.h>
#include <TCanvas.h>
#include <TFile.h>
#include <TTree.h>
#include <TString.h>
#include<vector>
using namespace std;

void data_perfect(){
   
    Int_t           runID;
    Int_t           eventID;
    Int_t           hitcounts;
    std::vector<Int_t>           *trackID=0;
    std::vector<Int_t>           *planeID=0;
    std::vector<Double_t>        *Energy_deposition=0;
    std::vector<Double_t>        *x_global=0;
    std::vector<Double_t>        *y_global=0;
    std::vector<Double_t>        *z_global=0;
    std::vector<Double_t>        *x_local=0;
    std::vector<Double_t>        *y_local=0;
    std::vector<Double_t>        *z_local=0;

    TBranch        *b_runID;   //!
    TBranch        *b_eventID;   //!
    TBranch        *b_trackID;   //!
    TBranch        *b_hitcounts;   //!
    TBranch        *b_planeID;   //!
    TBranch        *b_Energy_deposition;   //!
    TBranch        *b_x_global;   //!
    TBranch        *b_y_global;   //!
    TBranch        *b_z_global;   //!
    TBranch        *b_x_local;   //!
    TBranch        *b_y_local;   //!
    TBranch        *b_z_local;   //!

    Int_t           t_runID;
    Int_t           t_eventID;
    Int_t           t_hitcounts;
    std::vector<Int_t> t_trackID;
    std::vector<Int_t> t_planeID;
    std::vector<Double_t> t_Energy_deposition;
    std::vector<Double_t> t_x_global;
    std::vector<Double_t> t_y_global;
    std::vector<Double_t> t_z_global;
    std::vector<Double_t> t_x_local;
    std::vector<Double_t> t_y_local;
    std::vector<Double_t> t_z_local;

    std::vector<Double_t> t_planehits(12,0);
    std::vector<Double_t> t_planegapx(12,0);
    std::vector<Double_t> t_planegapy(12,0);
    std::vector<Double_t> t_planegapz(12,0);

    std::vector<vector<Double_t>> t_planex(12);
    std::vector<vector<Double_t>> t_planey(12);
    std::vector<vector<Double_t>> t_planez(12);


    TFile *input = TFile::Open("taichu_150.root");
    TTree *t1 = (TTree*)input->Get("taichu_150");

    t1->SetBranchAddress("eventID", &eventID, &b_eventID);
    t1->SetBranchAddress("runID", &runID, &b_runID);
    t1->SetBranchAddress("hitcounts", &hitcounts, &b_hitcounts);
    t1->SetBranchAddress("trackID", &trackID);
    t1->SetBranchAddress("planeID", &planeID);
    t1->SetBranchAddress("Energy_deposition", &Energy_deposition);
    t1->SetBranchAddress("x_global", &x_global);
    t1->SetBranchAddress("y_global", &y_global);
    t1->SetBranchAddress("z_global", &z_global);
    t1->SetBranchAddress("x_local", &x_local);
    t1->SetBranchAddress("y_local", &y_local);
    t1->SetBranchAddress("z_local", &z_local);

    TFile *output = TFile::Open("perfect_event.root","RECREATE");//output
    TTree *t2 = new TTree("perfect_event","title");
    t2->Branch("runID",&t_runID);
    t2->Branch("eventID",&t_eventID);
    t2->Branch("hitcounts",&t_hitcounts);
    t2->Branch("trackID",&t_trackID);
    t2->Branch("planeID",&t_planeID);
    t2->Branch("Energy_deposition",&t_Energy_deposition);
    t2->Branch("x_global",&t_x_global);
    t2->Branch("y_global",&t_y_global);
    t2->Branch("z_global",&t_z_global);
    t2->Branch("x_local",&t_x_local);
    t2->Branch("y_local",&t_y_local);
    t2->Branch("z_local",&t_z_local);
    t2->Branch("planehits",&t_planehits);
    t2->Branch("planegapx",&t_planegapx);
    t2->Branch("planegapy",&t_planegapy);
    t2->Branch("planegapz",&t_planegapz);
    //output->cd();
    //t2->Write();
    //output->Close();
	
    Int_t num12 = 0;
    Int_t new_events = 0;
    for(Int_t i=0;i<t1->GetEntries();i++){
        t1->GetEntry(i);
        if(hitcounts!=12){
            continue;
        }
        num12+=1;

        //if(i==0){
        //    (*planeID)[10] = (*planeID)[11];
        //}
        //check whether can find almost perfect event,ignore in normal running


        std::unordered_map<int,int> t_map;
        bool flag = true;
        for(int i = 0;i<12;i++) t_map[i]=0;
        for(int i = 0;i<12;i++){
            t_map[(*planeID)[i]]^=1;
            if(t_map[(*planeID)[i]]==0){
                flag = false;
                cout<<"Find almost perfect event"<<endl;
                break;
            }
        } 
        if(flag == false) continue;


        t_trackID.clear();
        t_planeID.clear();
        t_Energy_deposition.clear();
        t_x_global.clear();
        t_y_global.clear();
        t_z_global.clear();
        t_x_local.clear();
        t_y_local.clear();
        t_z_local.clear();


        t_planehits.assign(12,0);
        t_planegapx.assign(12,0);
        t_planegapy.assign(12,0);
        t_planegapz.assign(12,0);

        for(int j=0;j<12;j++){
            t_planex[j].clear();
            t_planey[j].clear();
            t_planez[j].clear();
        }

        t_eventID=new_events++;
        t_runID = runID;
        t_hitcounts=hitcounts;
        for(int j=0;j<trackID->size();j++){
            t_trackID.push_back((*trackID)[j]);
            t_planeID.push_back((*planeID)[j]);
            t_Energy_deposition.push_back((*Energy_deposition)[j]);
            t_x_global.push_back((*x_global)[j]);
            t_y_global.push_back((*y_global)[j]);
            t_z_global.push_back((*z_global)[j]);
            t_x_local.push_back((*x_local)[j]);
            t_y_local.push_back((*y_local)[j]);
            t_z_local.push_back((*z_local)[j]);
            t_planehits[(*planeID)[j]]+=1;

            t_planex[(*planeID)[j]].push_back((*x_global)[j]);
            t_planey[(*planeID)[j]].push_back((*y_global)[j]);
            t_planez[(*planeID)[j]].push_back((*z_global)[j]);

        }

        for(int j=0;j<12;j++){
            t_planegapx[j]= *max_element(t_planex[j].begin(),t_planex[j].end())-*min_element(t_planex[j].begin(),t_planex[j].end());
            t_planegapy[j]= *max_element(t_planey[j].begin(),t_planey[j].end())-*min_element(t_planey[j].begin(),t_planey[j].end());
            t_planegapz[j]= *max_element(t_planez[j].begin(),t_planez[j].end())-*min_element(t_planez[j].begin(),t_planez[j].end());
            //cout<<t_planehits[j]<<endl;
        }
        

        //cout<<"eventID:"<<t_eventID<<endl;
        //cout<<"hitcounts:"<<t_hitcounts<<endl;
        //cout<<endl;
        //cout<<t_x_global.size()<<endl;
        t2->Fill();
    }
    cout<<"12hit events:"<<num12<<endl;
    cout<<"perfect events:"<<t_eventID+1<<endl;
    output->cd();
    t2->Write();
    output->Close();

}