
#include <TH2.h>
#include <TStyle.h>
#include <TCanvas.h>
#include <TFile.h>
#include <TChain.h>
#include <TTree.h>
#include <TString.h>
#include<vector>
using namespace std;

const double pixel_sizex = 0.025;
const double pixel_sizey = 0.025;

void data_arrange(){

    double chip_thick = 0.15;
   
    Int_t           eventID;
    Int_t           trcID;
    Int_t           planeID;
    Double_t        Energy_deposition;
    Double_t        x;
    Double_t        y;
    Double_t        z;
    Double_t        x_local;
    Double_t        y_local;
    Double_t        z_local;
    Double_t        x_global;
    Double_t        y_global;
    Double_t        z_global;
    Int_t           runID;
    Int_t           planeID_discrete;
    Double_t        x_local_discrete;
    Double_t        y_local_discrete;


    TBranch        *b_eventID;   //!
    TBranch        *b_trcID;   //!
    TBranch        *b_planeID;   //!
    TBranch        *b_Energy_deposition;   //!
    TBranch        *b_x;   //!
    TBranch        *b_y;   //!
    TBranch        *b_z;   //!
    TBranch        *b_x_local;   //!
    TBranch        *b_y_local;   //!
    TBranch        *b_z_local;   //!
    TBranch        *b_x_global;   //!
    TBranch        *b_y_global;   //!
    TBranch        *b_z_global;   //!
    TBranch        *b_runID;   //!
    TBranch        *b_planeID_discrete;
    TBranch        *b_x_local_discrete;
    TBranch        *b_y_local_discrete;


    std::vector<Int_t> t_trcID;
    std::vector<Int_t> t_planeID;
    std::vector<Double_t> t_Energy_deposition;
    std::vector<Double_t> t_x;
    std::vector<Double_t> t_y;
    std::vector<Double_t> t_z;
    std::vector<Double_t> t_x_local;
    std::vector<Double_t> t_y_local;
    std::vector<Double_t> t_z_local;
    std::vector<Double_t> t_x_global;
    std::vector<Double_t> t_y_global;
    std::vector<Double_t> t_z_global;
    std::vector<Int_t>            t_planeID_discrete;
    std::vector<Double_t>         t_x_local_discrete;
    std::vector<Double_t>         t_y_local_discrete;

    Int_t t_eventID = -2;
    Int_t t_hitcounts = -1;
    Int_t t_hitcounts_discrete = -1;
    Int_t t_runID = -1;

    
    TChain* t1 = new TChain("B2");
    t1->Add("./test*.root");

    //TFile *input = TFile::Open("test2.root");
    //TTree *t1 = (TTree*)input->Get("B2");

    t1->SetBranchAddress("eventID", &eventID, &b_eventID);
    t1->SetBranchAddress("trcID", &trcID, &b_trcID);
    t1->SetBranchAddress("planeID", &planeID, &b_planeID);
    t1->SetBranchAddress("Energy_deposition", &Energy_deposition, &b_Energy_deposition);
    t1->SetBranchAddress("x", &x, &b_x);
    t1->SetBranchAddress("y", &y, &b_y);
    t1->SetBranchAddress("z", &z, &b_z);
    t1->SetBranchAddress("x_local", &x_local, &b_x_local);
    t1->SetBranchAddress("y_local", &y_local, &b_y_local);
    t1->SetBranchAddress("z_local", &z_local, &b_z_local);
    t1->SetBranchAddress("x_global", &x_global, &b_x_global);
    t1->SetBranchAddress("y_global", &y_global, &b_y_global);
    t1->SetBranchAddress("z_global", &z_global, &b_z_global);
    t1->SetBranchAddress("runID", &runID, &b_runID);


    TFile *output = TFile::Open("taichu_150.root","RECREATE");//output
    TTree *t2 = new TTree("taichu_150","title");
    t2->Branch("eventID",&t_eventID);
    t2->Branch("hitcounts",&t_hitcounts);
    t2->Branch("hitcounts_discrete",&t_hitcounts_discrete);
    t2->Branch("trackID",&t_trcID);
    t2->Branch("planeID",&t_planeID);
    t2->Branch("Energy_deposition",&t_Energy_deposition);
    t2->Branch("x",&t_x);
    t2->Branch("y",&t_y);
    t2->Branch("z",&t_z);
    t2->Branch("x_local",&t_x_local);
    t2->Branch("y_local",&t_y_local);
    t2->Branch("z_local",&t_z_local);
    t2->Branch("x_global",&t_x_global);
    t2->Branch("y_global",&t_y_global);
    t2->Branch("z_global",&t_z_global);
    t2->Branch("x_local_discrete",&t_x_local_discrete);
    t2->Branch("y_local_discrete",&t_y_local_discrete);
    t2->Branch("runID",&t_runID);

    Int_t cur = -1;
    std::unordered_set<string> t_set;
    for(Int_t i=0;i<t1->GetEntries();i++){
        t1->GetEntry(i);
        if(eventID!=-1){
            t_eventID=eventID;
            t_runID = runID;
            t_hitcounts = t_planeID.size();
            t_hitcounts_discrete = t_planeID_discrete.size();
            t2->Fill();
            t_trcID.clear();
            t_planeID.clear();
            t_Energy_deposition.clear();
            t_x.clear();
            t_y.clear();
            t_z.clear();
            t_x_local.clear();
            t_y_local.clear();
            t_z_local.clear();
            t_x_global.clear();
            t_y_global.clear();
            t_z_global.clear();
            t_planeID_discrete.clear();
            t_x_local_discrete.clear();
            t_y_local_discrete.clear();
            t_set.clear();
            continue;
        }
        double xl = int((x_local/pixel_sizex))*pixel_sizex+pixel_sizex/2;
        double yl = int((y_local/pixel_sizey))*pixel_sizey+pixel_sizey/2;
        if(t_set.find(to_string(xl)+"+"+to_string(yl)+"+"+to_string(planeID))!=t_set.end()){
            
        }else{
            t_x_local_discrete.push_back(xl);
            t_y_local_discrete.push_back(yl);
            t_planeID_discrete.push_back(planeID);
            t_set.insert(to_string(xl)+"+"+to_string(yl)+"+"+to_string(planeID));
        }

        //if(Energy_deposition<0.03) continue;
        t_trcID.push_back(trcID);
        t_planeID.push_back(planeID);
        t_Energy_deposition.push_back(Energy_deposition);
        t_x.push_back(x);
        t_y.push_back(y);
        t_z.push_back(z);
        t_x_local.push_back(x_local);
        t_y_local.push_back(y_local);
        t_z_local.push_back(z_local);
        t_x_global.push_back(x_global);
        t_y_global.push_back(y_global);
        t_z_global.push_back(z_global);



    }
    output->cd();
    t2->Write();
    output->Close();

}