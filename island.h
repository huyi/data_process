#ifndef ISLAND_H
#define ISLAND_H
#include <vector>
#include <math.h>
//only run in root, if want expend plz write makefile
const Double_t pixelsize = 0.025;

const double pixel_sizex = 0.025;
const double pixel_sizey = 0.025;

using namespace std;
class island{
    private:
    int numOfislands = 0;
    vector<pair<double,double>> island_center;//pair for output
    vector<pair<double,double>> t_island;//pair for temp use


    public:
    island();
    island(const vector<double> &x,const vector<double> &y);
    //~island();
    int getnumOfislands(){return numOfislands;}
    vector<pair<double,double>> getcenterOfislands(){return island_center;}

    void dfs(std::unordered_set<int> &t_set,const vector<vector<int>> &id,int i,int j);
    bool inArea(const vector<vector<int>> &id,int i,int j);
    bool inLink(const vector<vector<int>> &id,int i,int j,int ii, int jj);
    
};

island::island(const vector<double> &x,const vector<double> &y){
    vector<vector<int>> t_island_id;
    std::unordered_set<int> island_tag;
    //fill pair
    for (int k = 0; k < x.size(); k++){
        t_island.push_back(make_pair(x[k],y[k]));
    }
    //sort from low to high, x first
    sort(t_island.begin(),t_island.end(),[](const pair<double,double>& a, const pair<double,double>& b){
         if (a.first < b.first) return true;
         else if(a.first == b.first){
            return a.second<b.second;
         }
         else return false;
    });
    //fill id vector
    int numid = 0;
    if(t_island.size()!=0) t_island_id.push_back({0});
    for(int i = 1;i<t_island.size();i++){
        if(t_island[i].first!=t_island[i-1].first){
            t_island_id.push_back({i});
            numid++;
        }else{
            t_island_id[numid].push_back(i);
        }
    }
    //find island
    for(int i=0;i<t_island_id.size();i++){
        for(int j=0;j<t_island_id[i].size();j++){
            if(island_tag.find(t_island_id[i][j])==island_tag.end()){
                dfs(island_tag,t_island_id,i,j);
                numOfislands++;
            }
        }
    }
}

void island::dfs(std::unordered_set<int> &t_set,const vector<vector<int>> &id,int i,int j){
    // end situation: over limitation
    if(!inArea(id,i,j))
        return;
    // if checked return
    if(t_set.find(id[i][j])!=t_set.end())
        return;
    t_set.insert(id[i][j]);// tag
    
    if(inLink(id,i,j,i,j+1))    dfs(t_set,id,i,j+1);// up   
    if(inLink(id,i,j,i,j-1))    dfs(t_set,id,i,j-1);// down
    if(inLink(id,i,j,i-1,j))    dfs(t_set,id,i-1,j);// l
    if(inLink(id,i,j,i+1,j))    dfs(t_set,id,i+1,j);// r
}

bool island::inArea(const vector<vector<int>> &id,int i,int j){
    return i >= 0 && i < id.size() && j >= 0 && j<id[i].size();
}

bool island::inLink(const vector<vector<int>> &id,int i,int j,int ii, int jj){
    if(!inArea(id,i,j)||!inArea(id,ii,jj)) return false;
    return abs(t_island[id[i][j]].first-t_island[id[ii][jj]].first)<pixel_sizex || abs(t_island[id[i][j]].second-t_island[id[ii][jj]].second)<pixel_sizey;
}
#endif


