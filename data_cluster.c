
#include <TH2.h>
#include <TStyle.h>
#include <TCanvas.h>
#include <TFile.h>
#include <TTree.h>
#include <TString.h>
#include <vector>
#include "island.h"
using namespace std;



void data_cluster()
{
    Int_t eventID;
    Int_t hitcounts;
    std::vector<Int_t> *planeID = 0;

    std::vector<Double_t> *x_global = 0;
    std::vector<Double_t> *y_global = 0;
    std::vector<Double_t> *z_global = 0;

    std::vector<Double_t> *x_local = 0;
    std::vector<Double_t> *y_local = 0;

    TBranch *b_eventID;   //!
    TBranch *b_hitcounts; //!
    TBranch *b_planeID;   //!
    TBranch *b_x_global;  //!
    TBranch *b_y_global;  //!
    TBranch *b_z_global;  //!
    TBranch *b_x_local;   //!
    TBranch *b_y_local;   //!

    Int_t t_eventID;
    Int_t t_numOfcluster;
    Int_t t_numOfcluster_discrete;
    std::vector<Int_t> t_planeID;
    std::vector<Int_t> t_planeID_discrete;


    std::vector<Double_t> t_x_global;
    std::vector<Double_t> t_y_global;
    std::vector<Double_t> t_z_global;

    std::vector<Double_t> t_x_local;
    std::vector<Double_t> t_y_local;

    std::vector<Double_t> t_x_local_discrete;
    std::vector<Double_t> t_y_local_discrete;

    std::vector<Int_t> t_planehits(12, 0);
    std::vector<Double_t> t_planegapx(12, 0);
    std::vector<Double_t> t_planegapy(12, 0);

    std::vector<vector<Double_t>> t_planex_global(12);
    std::vector<vector<Double_t>> t_planey_global(12);
    std::vector<vector<Double_t>> t_planez_global(12);

    std::vector<vector<Double_t>> t_planex_local(12);
    std::vector<vector<Double_t>> t_planey_local(12);

    std::vector<vector<Double_t>> t_planex_local_discrete(12);
    std::vector<vector<Double_t>> t_planey_local_discrete(12);

    vector<int> tt_planeID;
    vector<int> tt_planeID_discrete;

    vector<double> tt_x_global;
    vector<double> tt_y_global;
    vector<double> tt_z_global;
    vector<double> tt_x_local;
    vector<double> tt_y_local;
    vector<double> tt_x_local_discrete;
    vector<double> tt_y_local_discrete;
    vector<pair<double,double>> tt_xy;
    std::unordered_set<string> tt_set;

    TFile *input = TFile::Open("taichu_150.root");
    TTree *t1 = (TTree *)input->Get("taichu_150");

    t1->SetBranchAddress("eventID", &eventID, &b_eventID);
    t1->SetBranchAddress("hitcounts", &hitcounts, &b_hitcounts);
    t1->SetBranchAddress("planeID", &planeID);

    t1->SetBranchAddress("x_global", &x_global);
    t1->SetBranchAddress("y_global", &y_global);
    t1->SetBranchAddress("z_global", &z_global);

    t1->SetBranchAddress("x_local", &x_local);
    t1->SetBranchAddress("y_local", &y_local);

    //t1->SetBranchAddress("x_local_disrete", &x_local_disrete);
    //t1->SetBranchAddress("y_local_disrete", &y_local_disrete);

    TFile *output = TFile::Open("cluster.root", "RECREATE"); // output
    TTree *t2 = new TTree("cluster", "title");

    t2->Branch("eventID", &t_eventID);
    t2->Branch("numOfcluster", &t_numOfcluster);
    t2->Branch("numOfcluster_discrete", &t_numOfcluster_discrete);
    t2->Branch("planeID", &t_planeID);
    t2->Branch("planeID_discrete", &t_planeID_discrete);


    t2->Branch("x_global", &t_x_global);
    t2->Branch("y_global", &t_y_global);
    t2->Branch("z_global", &t_z_global);

    t2->Branch("x_local", &t_x_local);
    t2->Branch("y_local", &t_y_local);

    t2->Branch("x_local_discrete", &t_x_local_discrete);
    t2->Branch("y_local_discrete", &t_y_local_discrete);

    t2->Branch("planehits", &t_planehits);
    t2->Branch("planegapx", &t_planegapx);
    t2->Branch("planegapy", &t_planegapy);

    Int_t id = 0;
    Int_t maxdraw = 10;
    TH2F *muti_hit[maxdraw];
    TH2F *muti_hit_continuous[maxdraw];

    for (int m = 0; m < maxdraw; m++){
        TString nameX = "muti_hit_discrete" + to_string(m);
        muti_hit[m] = new TH2F(nameX,"",800,-10,10,800,-10,10);
        muti_hit[m]->SetMarkerColor(1);
        muti_hit[m]->SetMarkerSize(2);
        muti_hit[m]->SetMarkerStyle(7);
        //muti_hit[m]->SetLineColor(6);
        //muti_hit[m]->SetLineWidth(603);
        TString nameX2 = "muti_hit" + to_string(m);
        muti_hit_continuous[m] = new TH2F(nameX2,"",800,-10,10,800,-10,10);
        muti_hit_continuous[m]->SetMarkerColor(1);
        muti_hit_continuous[m]->SetMarkerSize(2);
        muti_hit_continuous[m]->SetMarkerStyle(7);   
    }

    for (Int_t i = 0; i < t1->GetEntries(); i++){
        t1->GetEntry(i);

        // temp variable clear and reset
        t_planeID.clear();
        t_planeID_discrete.clear();

        t_x_global.clear();
        t_y_global.clear();
        t_z_global.clear();

        t_x_local.clear();
        t_y_local.clear();

        t_x_local_discrete.clear();
        t_y_local_discrete.clear();

        t_planehits.assign(12, 0);
        t_planegapx.assign(12, 0);
        t_planegapy.assign(12, 0);

        for (int j = 0; j < 12; j++)
        {
            t_planex_global[j].clear();
            t_planey_global[j].clear();
            t_planez_global[j].clear();

            t_planex_local[j].clear();
            t_planey_local[j].clear();

            t_planex_local_discrete[j].clear();
            t_planey_local_discrete[j].clear();
        }
        // fill temp var
        for (int j = 0; j < planeID->size(); j++)
        {
            t_planehits[(*planeID)[j]] += 1;

            t_planex_global[(*planeID)[j]].push_back((*x_global)[j]);
            t_planey_global[(*planeID)[j]].push_back((*y_global)[j]);
            t_planez_global[(*planeID)[j]].push_back((*z_global)[j]);

            t_planex_local[(*planeID)[j]].push_back((*x_local)[j]);
            t_planey_local[(*planeID)[j]].push_back((*y_local)[j]);
        }

        for (int j = 0; j < 12; j++)
        {   
            // draw judge 
            auto draw_f = [&](){
                return id<maxdraw&&t_planex_local[j].size()>25;
            };
            // save gap first
            t_planegapx[j] = *max_element(t_planex_local[j].begin(), t_planex_local[j].end()) - *min_element(t_planex_local[j].begin(), t_planex_local[j].end());
            t_planegapy[j] = *max_element(t_planey_local[j].begin(), t_planey_local[j].end()) - *min_element(t_planey_local[j].begin(), t_planey_local[j].end());

            // temp var to save cluster data
            tt_planeID.clear();
            tt_planeID_discrete.clear();
            tt_x_global.clear();
            tt_y_global.clear();
            tt_z_global.clear();
            tt_x_local.clear();
            tt_y_local.clear();
            tt_x_local_discrete.clear();
            tt_y_local_discrete.clear();
            tt_xy.clear();
            tt_set.clear();

            ////fill pair
            //for (int k = 0; k < t_planex_local[j].size(); k++){
            //    tt_xy.push_back(make_pair(t_planex_local[j][k],t_planey_local[j][k]));
            //}
            ////sort from low to high, x first
            //sort(tt_xy.begin(),tt_xy.end(),[](const pair<int, int>& a, const pair<int, int>& b){
            //     if (a.first < b.first) return true;
            //     else if(a.first == b.first){
            //        return a.second<b.second;
            //     }
            //     else return false;
            //});
            
            

            //fill data by planeID and try hit->cluster
            if (t_planex_local[j].size() == 1) {
                tt_planeID.push_back(j);
                tt_x_global.push_back(t_planex_global[j][0]);
                tt_y_global.push_back(t_planey_global[j][0]);
                tt_z_global.push_back(t_planez_global[j][0]);
                tt_x_local.push_back(t_planex_local[j][0]);
                tt_y_local.push_back(t_planey_local[j][0]);
                double xl = int((t_planex_local[j][0]/pixel_sizex))*pixel_sizex+pixel_sizex/2;
                double yl = int((t_planey_local[j][0]/pixel_sizey))*pixel_sizey+pixel_sizey/2;
                if(tt_set.find(to_string(xl)+"+"+to_string(yl))!=tt_set.end()){

                }else{
                    tt_planeID_discrete.push_back(j);
                    tt_x_local_discrete.push_back(xl);
                    tt_y_local_discrete.push_back(yl);
                    tt_set.insert(to_string(xl)+"+"+to_string(yl));
                }

            }
            //more than 1 hit
            else{
                //// draw judge 
                //auto draw_f = [&](){
                //    return id<maxdraw&&t_planex_local[j].size()>25;
                //};
                //draw scatter pics
                for (int k = 0; k < t_planex_local[j].size(); k++){
                    tt_planeID.push_back(j);
                    tt_x_global.push_back(t_planex_global[j][k]);
                    tt_y_global.push_back(t_planey_global[j][k]);
                    tt_z_global.push_back(t_planez_global[j][k]);
                    tt_x_local.push_back(t_planex_local[j][k]);
                    tt_y_local.push_back(t_planey_local[j][k]);
                    double xl = int((t_planex_local[j][k]/pixel_sizex))*pixel_sizex+pixel_sizex/2;
                    double yl = int((t_planey_local[j][k]/pixel_sizey))*pixel_sizey+pixel_sizey/2;
                    if(tt_set.find(to_string(xl)+"+"+to_string(yl))!=tt_set.end()){

                    }else{
                        tt_planeID_discrete.push_back(j);
                        tt_x_local_discrete.push_back(xl);
                        tt_y_local_discrete.push_back(yl);
                        tt_set.insert(to_string(xl)+"+"+to_string(yl));
                        if(draw_f()){
                            cout<<"x,y:"<<xl<<","<<yl<<endl;                    
                            muti_hit[id]->Fill(xl,yl);
                            //cout<<"x,y:"<<int((t_planex_local[j][k]/pixel_sizex))<<","<<int((t_planey_local[j][k]/pixel_sizey))<<endl;                    
                            //muti_hit[id]->Fill(int((t_planex_local[j][k]/pixel_sizex)),int((t_planey_local[j][k]/pixel_sizey)));
                        }
                    }
                    if(draw_f()){
                        muti_hit_continuous[id]->Fill(t_planex_local[j][k],t_planey_local[j][k]);
                    }
                }
                if(draw_f()){
                    cout<<"id"<<id<<endl;
                    id++;
                }          
            }
            //try to solve it
            island *t_island = new island(tt_x_local_discrete,tt_y_local_discrete);
            if(draw_f())    cout<<t_island->getnumOfislands()<<endl;
            delete t_island;
            

            // Fill processed cluster data
            for (int k = 0; k < tt_planeID.size(); k++)
            {
                t_planeID.push_back(tt_planeID[k]);
                t_x_global.push_back(tt_x_global[k]);
                t_y_global.push_back(tt_y_global[k]);
                t_z_global.push_back(tt_z_global[k]);
                t_x_local.push_back(tt_x_local[k]);
                t_y_local.push_back(tt_y_local[k]);
                
            }

            for (int k = 0; k < tt_x_local_discrete.size(); k++)
            {
                t_planeID_discrete.push_back(tt_planeID_discrete[k]);
                t_x_local_discrete.push_back(tt_x_local_discrete[k]);
                t_y_local_discrete.push_back(tt_y_local_discrete[k]);
            }
        }

        t_eventID = eventID;
        t_numOfcluster = t_x_local.size();
        t_numOfcluster_discrete = t_x_local_discrete.size();

        // cout<<"eventID:"<<t_eventID<<endl;
        // cout<<endl;

        t2->Fill();
    }

    output->cd();
    t2->Write();
    for(int i=0;i<maxdraw;i++){
        muti_hit[i]->Write();
        muti_hit_continuous[i]->Write();

    }
    output->Close();
}


